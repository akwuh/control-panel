define([
    'underscore',
    'backbone',
], function(_, Backbone, Model) {
    'use strict';

    var Collection = Backbone.Collection.extend({

        parse: function(d) {
            console.log(d);
            this.loadMoreUrl = _.property('url')(_.findWhere(d && d.meta && d.meta.links, {
                'rel': 'next'
            }));
            return d.data;
        }

    });

    return Collection;

});
