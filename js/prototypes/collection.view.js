define([
    'jquery',
    'underscore',
    'backbone',
    'mixins/ui'
], function($, _, Backbone, UIMixin) {
    'use strict';

    var View = Backbone.View.extend({

        initialize: function() {
            this.listenTo(this.collection, 'load', _.compose(
                this.showLoader, this.hideLoadMoreButton));
            this.listenTo(this.collection, 'sync', _.compose(
                this.update, this.hideLoader, this.showLoadMoreButton));
            this.listenTo(this.collection, 'error', this.fallback);
        },

        fallback: function() {
            if (this.fallbackUrl === false) {
                return;
            } else if (!this.fallbackUrl) {
                window.location.href = '../login';
            } else {
                Materialize.toast('Страница не найдена', 2000);
                window.location.hash = this.fallbackUrl;
            }
        },

        events: {
            'click .forward-url': 'forwardUrl',
            'click .load-more': 'loadMore'
        },

        forwardUrl: function(e) {
            var url = $(e.currentTarget).attr('data-url');
            if (_.isUndefined) {
                window.location.hash = url;
            }
        },

        loadMore: function() {
            if (!this.collection.loadMoreUrl) return;
            this.collection.fetch({
                'url': this.collection.loadMoreUrl,
                'remove': false
            });
            this.collection.trigger('load');
        },

        render: function() {
            var $el = this.$el;
            this.setElement(this.template());
            $el.replaceWith(this.$el);
            return this;
        },

        update: function() {
            var fragment = document.createDocumentFragment();
            this.collection.each(function(m) {
                var view = new this.ItemView({
                    model: m,
                    template: this.itemTemplate
                });
                fragment.appendChild(view.render().el);
            }, this);
            this.$('#items').html(fragment);

            this.switchEmptyCard(!this.collection.length);
            this.collection.loadMoreUrl ? this.showLoadMoreButton() : this.hideLoadMoreButton();
        }

    });

    _.defaults(View.prototype, UIMixin);

    return View;

});
