define([
    'jquery',
    'underscore',
    'backbone',
    'mixins/ui'
], function($, _, Backbone, UIMixin) {
    'use strict';

    var View = Backbone.View.extend({

        initialize: function(options) {
            this.activeTemplate = options.template || this.template;
            this.listenTo(this.model, 'request', this.showLoader);
            this.listenTo(this.model, 'sync', this.hideLoader);
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'error', _.compose(this.hideLoader, this.fallback));
        },

        events: {
            'click .forward-url': 'forwardUrl'
        },

        fallback: function() {
            if (this.fallbackUrl === false) {
                return;
            } else if (!this.fallbackUrl) {
                window.location.href = '../login';
            } else {
                Materialize.toast('Страница не найдена', 2000);
                window.location.hash = this.fallbackUrl;
            }
        },

        forwardUrl: function(e) {
            var url = $(e.currentTarget).attr('data-url');
            if (_.isUndefined) {
                window.location.hash = url;
            }
        },

        render: function() {
            var $el = this.$el;
            this.setElement(this.activeTemplate({
                'model': this.model
            }));
            $el.replaceWith(this.$el);
            $.fn.materialize();
            return this;
        }

    });

    _.defaults(View.prototype, UIMixin);

    return View;

});
