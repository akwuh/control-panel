'use strict';

require.config({
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		},
		materialize: {
			deps: ['jquery'],
			exports: ['materialize']
		}
	},
	paths: {
		'jquery': 'vendor/jquery',
		'underscore': 'vendor/underscore',
		'backbone': 'vendor/backbone',
		'text': 'vendor/requirejs-text',
		'materialize': 'vendor/materialize',
		'mockjax': 'vendor/jquery.mockjax',
		'model': 'prototypes/model',
		'model.view': 'prototypes/model.view',
		'collection': 'prototypes/collection',
		'collection.view': 'prototypes/collection.view'
	}
});

require([
	'underscore',
	'backbone',
	'views/application/shop',
	'routers/shop',
	'materialize',
	'mixins/plugins.jquery'
], function (_, Backbone, AppView, Router) {
	(new AppView).render();
	_.defer(function(){
		new Router();
	});

});