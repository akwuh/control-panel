define([
    'jquery',
    'underscore',
    'collection.view',
    'views/product/item',
    'text!templates/product/collection.html',
    'text!templates/product/collection.item.html'
], function($, _, Prototype, ItemView, template, itemTemplate) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        itemTemplate: _.template(itemTemplate),
        ItemView: ItemView

    });

    return View;
});
