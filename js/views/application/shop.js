define([
	'jquery',
	'backbone',
	'text!templates/application/shop.html'
], function ($, Backbone, appTemplate) {
	'use strict';

	var View = Backbone.View.extend({

		template: _.template(appTemplate),
		
		render: function () {
			this.$el.html(this.template());
			$('body').html(this.el);
		}

	});

	return View;
});