define([
    'jquery',
    'underscore',
    'collection.view',
    'views/review/item',
    'text!templates/review/collection.html',
    'text!templates/review/collection.item.html'
], function($, _, Prototype, ItemView, template, itemTemplate) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        itemTemplate: _.template(itemTemplate),
        ItemView: ItemView

    });

    return View;
});
