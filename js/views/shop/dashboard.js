define([
    'jquery',
    'underscore',
    'model.view',
    'text!templates/shop/dashboard.html'
], function($, _, Prototype, template) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        fallbackUrl: ''

    });

    return View;
    
});
