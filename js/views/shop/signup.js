define([
    'jquery',
    'underscore',
    'backbone',
    'views/application/shop',
    'text!templates/shop/signup.html'
], function($, _, Backbone, App, template) {
    'use strict';

    var View = Backbone.View.extend({

        template: _.template(template),

        events: {
            'click .signup button': 'signup',
            'keypress input': 'keypress'
        },

        keypress: function(e) {
            this.$('.error').fadeOut();
            if (e.keyCode == 13) this.signup();
        },

        signup: function() {
            this.$('.signup').hide();
            this.$('.loader').show();
            var view = this;
            $.ajax({
                method: 'POST',
                url: '/signup',
                data: {
                    'email': $('#email').val(),
                    'name': $('#name').val(),
                    'url': $('#url').val(),
                    'role': 'SHOP'
                },
                success: function(r) {
                    // save Auth header
                    $('#signup-form').fadeOut(function(){
                        $('#signup-check-email').fadeIn();
                    });
                },
                error: function() {
                    view.$('.loader').hide();
                    view.$('.signup').show();
                    view.$('.error').fadeIn();
                }

            });
        },

        render: function() {
            this.$el.html(this.template());
            return this;
        }

    });

    return View;

});
