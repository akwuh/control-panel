define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/shop/login.html'
], function($, _, Backbone, template) {
    'use strict';

    var View = Backbone.View.extend({

        template: _.template(template),

        events: {
            'click .login button': 'login',
            'keypress input': 'keypress'
        },

        keypress: function(e) {
            this.$('.error').fadeOut();
            if (e.keyCode == 13) this.login();
        },

        login: function(e) {
            e.preventDefault();
            this.$('.login').hide();
            this.$('.loader').show();
            var view = this;
            $.ajax({
                method: 'POST',
                url: '/login',
                data: {
                    'email': $('#email').val(),
                    'password': $('#password').val(),
                    'role': 'SHOP'
                },
                success: function(r) {
                    // save Auth header
                    window.location.href = 'shop.html';
                },
                error: function() {
                    view.$('.loader').hide();
                    view.$('.login').show();
                    view.$('.error').fadeIn();
                }

            });
        },

        render: function() {
            this.$el.html(this.template());
            return this;
        }

    });

    return View;

});
