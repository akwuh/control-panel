define([
    'jquery',
    'underscore',
    'collection.view',
    'views/article/item',
    'text!templates/article/collection.html',
    'text!templates/article/collection.item.html',
], function($, _, Prototype, ItemView, template, itemTemplate) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        itemTemplate: _.template(itemTemplate),
        ItemView: ItemView

    });

    return View;
});
