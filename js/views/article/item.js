define([
    'jquery',
    'underscore',
    'model.view',
    'text!templates/article/item.html'
], function($, _, Prototype, template) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        fallbackUrl: 'articles'

    });

    return View;
    
});
