define([
    'jquery',
    'underscore',
    'model.view'
], function($, _, Prototype) {
    'use strict';

    var View = Prototype.extend({

        fallbackUrl: 'emails'

    });

    return View;
    
});
