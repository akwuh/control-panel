define([
    'underscore',
    'model'
], function(_, Prototype) {
    'use strict';

    var Model = Prototype.extend({

        url: 'shop/',

        defaults: {
            id: null,
            registeredAt: null,
            email: null,
            name: null,
            url: null,
            reviewsCount: null,
            productsCount: null,
            settings: {
                emailsDelay: null
            }            
        }

    });

    return Model;
    
});
