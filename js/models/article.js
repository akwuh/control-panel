define([
    'underscore',
    'model'
], function(_, Prototype) {
    'use strict';

    var Model = Prototype.extend({

        urlRoot: 'articles',

        defaults: {
            id: null,
            title: null,
            promo: null,
            html: null
        }

    });

    return Model;
    
});
