define([
    'underscore',
    'model'
], function(_, Prototype) {
    'use strict';

    var Model = Prototype.extend({

        urlRoot: 'shopProducts',

        defaults: {
            id: null,
            name: null,
            originalName: null,
            reviewsCount: null
        }

    });

    return Model;
    
});
