define([
    'jquery',
    'backbone',
    'models/article',
    'models/product',
    'models/shop',
    'collections/articles',
    'collections/products',
    'collections/reviews',
    'collections/emails',
    'views/article/item',
    'views/product/item',
    'views/article/collection',
    'views/product/collection',
    'views/review/collection',
    'views/email/collection',
    'views/shop/dashboard',
    'views/shop/settings'
], function(
    $, Backbone,
    Article, Product, Shop, Articles, Products, Reviews, Emails,
    ArticleView, ProductView,
    ArticlesView, ProductsView, ReviewsView, EmailsView,
    DashboardView, SettingsView
) {
    'use strict';

    var Router = Backbone.Router.extend({

        initialize: function() {
            Backbone.history.start();
        },

        routes: {
            'a(rticles)(/)': 'showArticles',
            'products(/)': 'showProducts',
            'reviews(/)': 'showReviews',
            'emails(/)': 'showEmails',
            'a(rticles)/:id(/:title)(/)': 'showArticle',
            'products/:id(/:name)(/)': 'showProduct',
            '(/)': 'showDashboard',
            'settings(/)': 'showSettings'
        },

        route: function(route, name, callback) {
            var router = this;
            if (!callback) callback = this[name];

            var f = function() {
                router.clear();
                callback.apply(router, arguments);
            };

            var result = Backbone.Router.prototype.route.call(this, route, name, f);
            return result;
        },

        clear: function() {
            delete this.view, this.model, this.collection;
        },

        showArticles: function() {
            this.collection = new Articles();
            this.view = new ArticlesView({
                collection: this.collection
            });
            $('#container').html(this.view.render().el);
            this.collection.fetch({
                reset: true
            });
        },

        showArticle: function(id, title) {
            this.model = new Article({
                id: id,
                title: title
            });
            this.view = new ArticleView({
                model: this.model
            });
            $('#container').html(this.view.render().el);
            this.model.fetch();
        },

        showProducts: function() {
            this.collection = new Products();
            this.view = new ProductsView({
                collection: this.collection
            });
            $('#container').html(this.view.render().el);
            this.collection.fetch({
                reset: true
            });
        },

        showProduct: function(id, name) {
            this.model = new Product({
                id: id,
                name: name
            });
            this.view = new ProductView({
                model: this.model
            });
            var reviews = new Reviews;
            var reviewsView = new ReviewsView({
                collection: reviews
            });
            $('#container')
                .html(this.view.render().el)
                .append(reviewsView.render().el);
            reviews.product = this.model;
            reviews.fetch();
            this.model.fetch();
            this.model.reviews = reviews;
            this.view.reviews = reviewsView;
        },

        showReviews: function() {
            this.collection = new Reviews();
            this.view = new ReviewsView({
                collection: this.collection
            });
            $('#container').html(this.view.render().el);
            this.collection.fetch({
                reset: true
            });
        },

        showEmails: function() {
            this.collection = new Emails();
            this.view = new EmailsView({
                collection: this.collection
            });
            $('#container').html(this.view.render().el);
            this.collection.fetch({
                reset: true
            });
        },

        showSettings: function() {
            this.model = new Shop();
            this.view = new SettingsView({
                model: this.model
            });
            $('#container').html(this.view.render().el);
            this.model.fetch();
        },

        showDashboard: function() {
            this.model = new Shop();
            this.view = new DashboardView({
                model: this.model
            });
            $('#container').html(this.view.render().el);
            this.model.fetch();
        }

    });

    return Router;

});
