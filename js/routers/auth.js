define([
    'jquery',
    'backbone',
    'views/shop/login',
    'views/shop/signup',
    'views/shop/restore'
], function($, Backbone, LoginView, SignupView, RestoreView) {
    'use strict';

    var Router = Backbone.Router.extend({

        initialize: function() {
            Backbone.history.start();
        },

        routes: {
            '(/)': 'showLogin',
            'login(/)': 'showLogin',
            'signup(/)': 'showSignup',
            'restore(/)': 'showRestore'
        },

        route: function(route, name, callback) {
            var router = this;
            if (!callback) callback = this[name];

            var f = function() {
                router.clear();
                callback.apply(router, arguments);
            };

            var result = Backbone.Router.prototype.route.call(this, route, name, f);
            return result;
        },

        clear: function() {
            delete this.view, this.model, this.collection;
        },

        showLogin: function() {
            this.view = new LoginView();
            $('#container').html(this.view.render().el);
        },

        showSignup: function() {
            this.view = new SignupView();
            $('#container').html(this.view.render().el);
        },

        showRestore: function() {
            this.view = new RestoreView();
            $('#container').html(this.view.render().el);
        }

    });

    return Router;

});
