define([
    'jquery',
    'underscore',
    'materialize'
], function($, _) {
    'use strict';

    $.fn.disable = function() {
        $(this).attr('disabled', 'disabled');
    };

    $.fn.enable = function() {
        $(this).removeAttr('disabled');
    };

    $.fn.serializeObject = function() {
        return _.object(_.map(this.serializeArray(), function(e){
            return [e.name, e.value];
        }));
    };

    $.fn.deferShow = function() {
      _.defer(function(v) {
            $(v).show();
        }, this);
    };

    $.fn.deferHide = function() {
      _.defer(function(v) {
            $(v).hide();
        }, this);
    };

    $.fn.materialize = function(){
        _.defer(function(){
            Waves.displayEffect();
            Materialize.updateTextFields();
            $(".dropdown-button").dropdown();
            $('ul.tabs').tabs();
            $('.tooltipped').tooltip({
                delay: 300
            });
        });
    }

    return null;

});
