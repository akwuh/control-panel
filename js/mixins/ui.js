define([
    'jquery',
    'mixins/plugins.jquery'
], function($) {
    'use strict';

    var Mixin = {
        switchEmptyCard: function(c) {
            c ? this.$('.empty-card').deferShow() 
            : this.$('.empty-card').deferHide();
        },
        showLoadMoreButton: function() {
            this.$('.load-more').deferShow();
        },
        hideLoadMoreButton: function() {
            this.$('.load-more').deferHide();
        },
        showLoader: function() {
            this.$('.loader').last().deferShow();
        },
        hideLoader: function() {
            this.$('.loader').last().deferHide();
        }
    };

    return Mixin;
    
});
