var gulp = require('gulp'),
    open = require('gulp-open'),
    shell = require('gulp-shell'),
    merge = require('merge-stream'),
    uglify = require('gulp-uglify'),
    uglifycss = require('gulp-uglifycss'),
    webserver = require('gulp-webserver'),
    sass = require('gulp-sass'),
    del = require('del');

gulp.task('clean', function() {
    return del(['dist/']);
});

gulp.task('sass', function() {
    return gulp.src('./scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('img', ['clean'], function() {
    return gulp.src(['./img/**/*.*']).pipe(gulp.dest('dist/img/'));
});

gulp.task('js', ['clean'], function() {
    return gulp.src(['./js/**/*.*']).pipe(gulp.dest('dist/js/'));
});

gulp.task('sass:watch', function() {
    return gulp.watch('./scss/**/*.s?css', ['sass']);
});

gulp.task('js:watch', function() {
    return gulp.watch('./js/**/*.js', ['js']);
});

gulp.task('default', ['js', 'sass', 'img'], function() {
    var js = gulp
        .src('dist/css/*.css')
        .pipe(uglifycss({
            uglyComments: true
        }))
        .pipe(gulp.dest('dist/css'));

    var css = gulp
        .src('dist/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));

    return merge(js, css);
});