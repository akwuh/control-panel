define([
    'jquery',
    'underscore',
    'collection.view',
    'views/email/item',
    'text!templates/email/collection.html',
    'text!templates/email/collection.item.html'
], function($, _, Prototype, ItemView, template, itemTemplate) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        itemTemplate: _.template(itemTemplate),
        ItemView: ItemView

    });

    return View;
});
