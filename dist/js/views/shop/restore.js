define([
    'jquery',
    'underscore',
    'backbone',
    'views/application/shop',
    'text!templates/shop/restore.html'
], function($, _, Backbone, App, template) {
    'use strict';

    var View = Backbone.View.extend({

        template: _.template(template),

        events: {
            'click .restore button': 'restore',
            'keypress input': 'keypress'
        },

        keypress: function(e) {
            this.$('.error').fadeOut();
            if (e.keyCode == 13) this.restore();
        },

        restore: function() {
            this.$('.restore').hide();
            this.$('.loader').show();
            var view = this;
            $.ajax({
                method: 'POST',
                url: '/restore',
                data: {
                    'email': $('#email').val()
                },
                success: function(r) {
                    // save Auth header
                    $('#restore-form').fadeOut(function(){
                        $('#restore-check-email').fadeIn();
                    });
                },
                error: function() {
                    view.$('.loader').hide();
                    view.$('.restore').show();
                    view.$('.error').fadeIn();
                }

            });
        },

        render: function() {
            this.$el.html(this.template());
            return this;
        }

    });

    return View;

});
