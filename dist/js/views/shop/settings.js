define([
    'jquery',
    'underscore',
    'model.view',
    'text!templates/shop/settings.html'
], function($, _, Prototype, template) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        fallbackUrl: false,

        events: _.extend({}, Prototype.events, {
            'click .save button': 'save'
        }),

        save: function(e){
            e.preventDefault();
            var view = this;
            this.$('.save button').disable();
            this.model.save(this.$('form').serializeObject(), {
                url: this.model.url,
                success: function(){
                    view.model.trigger('sync');
                    Materialize.toast('Изменения сохранены!', 2000);
                },
                error: function(r){
                    view.model.trigger('error');
                    Materialize.toast('Пожалуйста, проверьте введенные данные!', 2000);
                },
                complete: function(){
                    view.$('.save button').enable();
                }
            });
        }

    });

    return View;
    
});
