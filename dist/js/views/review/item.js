define([
    'jquery',
    'underscore',
    'model.view',
    'text!templates/review/collection.item.html'
], function($, _, Prototype, itemTemplate) {
    'use strict';

    var View = Prototype.extend({

        itemTemplate: _.template(itemTemplate),
        fallbackUrl: 'reviews',

        events: _.extend({}, Prototype.events, {
            'click button.approve': 'approve',
            'click button.reject': 'reject'
        }),

        approve: function() {
            this.model.approve();
        },

        reject: function() {
            this.model.reject();
        }

    });

    return View;
    
});
