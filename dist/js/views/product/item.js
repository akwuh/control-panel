define([
    'jquery',
    'underscore',
    'model.view',
    'text!templates/product/item.html'
], function($, _, Prototype, template) {
    'use strict';

    var View = Prototype.extend({

        template: _.template(template),
        fallbackUrl: 'products'

    });

    return View;
    
});
