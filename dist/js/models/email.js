define([
    'underscore',
    'model'
], function(_, Prototype) {
    'use strict';

    var Model = Prototype.extend({

        urlRoot: 'emails',

        defaults: {
            id: null,
            createdAt: null,
            sendAt: null,
            wasSent: null,
            reciever: null,
            sender: null,
            productName: null,
            html: null
        }

    });

    return Model;

});
