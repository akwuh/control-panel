define([
    'underscore',
    'backbone',
    'model'
], function(_, Backbone, Prototype) {
    'use strict';

    var Model = Prototype.extend({

        urlRoot: 'reviews',

        defaults: {
            id: null,
            createdAt: null,
            moderationStatus: null,
            moderationShopStatus: null,
            displayName: null,
            productName: null,
            usageTime: null,
            isOwner: null,
            opinion: {
                pros: null,
                cons: null,
                total: null,
                rating: null,
                worthBuy: null
            }
        },

        isApproved: function() {
            return this.get('moderationShopStatus') == 'approved';
        },

        isRejected: function() {
            return this.get('moderationShopStatus') == 'rejected';
        },

        approve: function() {
            var self = this;
            return Backbone.sync('create', this, {
                url: this.url() + '/approvement',
                success: function(){
                    self.set('moderationShopStatus', 'approved');
                    self.trigger('sync');
                },
                error: function(){
                    Materialize.toast('Something goes wrong', 2000);
                }                
            });
        },

        reject: function() {
            var self = this;
            return Backbone.sync('delete', this, {
                url: this.url() + '/approvement',
                success: function(){
                    self.set('moderationShopStatus', 'rejected');
                    self.trigger('sync');
                },
                error: function(){
                    Materialize.toast('Something goes wrong', 2000);
                }                
            });
        }
    });

    return Model;

});
