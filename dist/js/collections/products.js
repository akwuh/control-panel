define([
    'underscore',
    'collection',
    'models/product'
], function(_, Prototype, Model) {
    'use strict';

    var Collection = Prototype.extend({

        url: 'shopProducts',
        model: Model

    });

    return Collection;

});
