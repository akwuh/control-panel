define([
    'underscore',
    'collection',
    'models/article'
], function(_, Prototype, Model) {
    'use strict';

    var Collection = Prototype.extend({

        url: 'articles',
        model: Model

    });

    return Collection;
    
});
