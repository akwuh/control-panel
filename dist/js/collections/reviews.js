define([
    'underscore',
    'collection',
    'models/review'
], function(_, Prototype, Model) {
    'use strict';

    var Collection = Prototype.extend({

        url: function() {
            if (this.product) {
                return 'reviews?productName=' + this.product.get('name');
            }
            return 'reviews';
        },
        model: Model,

        approved: function() {
            return this.where({
                'moderationShopStatus': 'approved'
            });
        },

        rejected: function() {
            return this.where({
                'moderationShopStatus': 'rejected'
            });
        },

        unmoderated: function() {
            return this.where({
                'moderationShopStatus': null
            });
        }

    });

    return Collection;

});
