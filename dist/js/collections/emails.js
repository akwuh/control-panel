define([
    'underscore',
    'collection',
    'models/email'
], function(_, Prototype, Model) {
    'use strict';

    var Collection = Prototype.extend({

        url: 'emails',
        model: Model,

        waiting: function() {
            return this.reject(function(m) {
                return !!m.get('wasSent');
            });
        },

        sent: function() {
            return this.where({
                'wasSent': true
            });
        }

    });

    return Collection;

});
